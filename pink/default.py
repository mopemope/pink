import os
from pink.template import File, Directory, TemplateSet

class DefaultTemplateSet(TemplateSet):
    
    def __init__(self, pkg_name):
        name = "Default"
        contents = [   
            File('CHANGES.txt', 'changes.txt'),
            File('LICENSE.txt', 'license.txt'),
            File('MANIFEST.in', 'manifest.txt'),
            File('README.rst', 'readme.txt'),
            File('setup.py', 'setup.txt'),
            Directory(pkg_name),
            Directory("docs"),
            Directory("tests"),
            File(os.path.join(pkg_name, '__init__.py'), 'empty.txt'),
        ]
        super(DefaultTemplateSet, self).__init__(name,  contents=contents)



