import pink
from optparse import OptionParser

def command():
    usage = "%prog [options] package_name"
    version = "pink version %s" % pink.__version__
    parser = OptionParser(usage=usage, version=version)

