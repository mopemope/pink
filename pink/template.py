import datetime
import os
from string import Template


def simple_template_render(template_file, context):
    template = Template(template_file.read())
    return template.substitute(**context)


class FSNode(object):
    """
    Base class for file system node types, e.g. File and Directory.
    """

    def __init__(self, path):
        self.path = path



class File(FSNode):
    """
    Represents a file who's content is based on a template.
    """

    parent = os.path.join(os.path.dirname(__file__), 'templates')

    def __init__(self, path, template=None, parent=None,
            renderer=simple_template_render, substitutions=True):
        super(File, self).__init__(path)
        self.template = template
        self.renderer = renderer
        if parent:
            self.parent = parent

    @property
    def template_path(self):
        if self.template:
            path = self.template
        else:
            path = self.path
        return os.path.join(self.parent, path)

    def copy_to(self, directory, context):
        file_path = os.path.join(directory, self.path)
        with open(file_path, 'w') as f:
            with open(self.template_path) as template_file:
                if substitutions:
                    f.write(self.renderer(template_file, context))
                else:
                    f.write(template_file.read())
                    


class Directory(FSNode):
    """
    Represents a directory.
    """

    def copy_to(self, directory, context):
        file_path = os.path.join(directory, self.path)
        os.mkdir(file_path)


class TemplateSet(object):
   
    def __init__(self, name, template_dir=None, contents=None):
       self.name = name
       self.template_dir = template_dir
       self.contents = contents
    
    def make(self, target_dir, context):        
        if not self.contents:
            return
        
        for content in self.contents:
            print "creating file:", os.path.join(self.template_dir, content.path)
            content.copy_to(target_dir, context)
            

