try:
    from setuptools import Extension, setup
except ImportError:
    from distutils.core import Extension, setup

import os
import sys
import os.path
import platform

def read(name):
    return open(os.path.join(os.path.dirname(__file__), name)).read()

setup(name="pink",
        version="0.0.1",
        description="Create a Python package template.",
        long_description=read("README.rst"),
        keywords="Package",
        author="Yutaka Matsubara",
        author_email="yutaka.matsubara@gmail.com",
        url="",
        license="MIT",
        platforms="ANY",
        packages=["pink"],
        entry_points={
            'console_scripts': [
                'pink = pink.commands:command',
            ],
        },
        classifiers=[
            "Intended Audience :: Developers",
            "Environment :: Console",
            "License :: OSI Approved :: MIT License",
            "Topic :: Software Development :: Libraries"
        ],
)


